#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_universal2100.mk \
    $(LOCAL_DIR)/full_universal2100_r.mk \
    $(LOCAL_DIR)/full_universal2100_r_mali.mk \
    $(LOCAL_DIR)/full_smdk2100_r.mk \
    $(LOCAL_DIR)/full_universal2100_r_evt0.mk

COMMON_LUNCH_CHOICES := \
    full_universal2100_r_evt0-eng \
    full_universal2100_r_evt0-userdebug \
    full_universal2100_r_evt0-user \
    full_universal2100_r-eng \
    full_universal2100_r-userdebug \
    full_universal2100_r-user \
    full_universal2100_r_mali-userdebug \
    full_smdk2100_r-eng \
    full_smdk2100_r-userdebug \
    full_smdk2100_r-user
