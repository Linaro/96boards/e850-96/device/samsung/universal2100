#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# TODO(b/147336716): Remove BUILD_BROKEN_PREBUILT_ELF_FILES
#BUILD_BROKEN_PREBUILT_ELF_FILES := true

# Should be uncommented after fixing vndk-sp violation is fixed.
PRODUCT_FULL_TREBLE_OVERRIDE := true

TARGET_LINUX_KERNEL_VERSION := 5.4
TARGET_BOARD_INFO_FILE := device/samsung/universal2100/board-info.txt

TARGET_SOC_NAME := exynos

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv8-a
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true

TARGET_CPU_VARIANT := generic
TARGET_CPU_VARIANT_RUNTIME := cortex-a76
TARGET_2ND_CPU_VARIANT := generic
TARGET_2ND_CPU_VARIANT_RUNTIME := cortex-a76

TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true

# SOC Model specific variable. For CDD12 Requirement
# BUG ID: 182328165
TARGET_SOC_MANUFACTURE := Samsung
TARGET_SOC_MODEL := Exynos 2100

# Add ro.board.first_api_level for vendor partition
# it must be added with BUG:180561519
# - VF chipsets MUST set required system properties to indicate the first vendor software level
BOARD_SHIPPING_API_LEVEL := 30


# UNIVERSAL common modules
BOARD_UNIVERSAL_COMMON_MODULES := liblight

OVERRIDE_RS_DRIVER := libRSDriverArm.so
BOARD_EGL_CFG := device/samsung/universal2100/conf/egl.cfg
#BOARD_USES_HGL := true
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
BOARD_USES_EXYNOS5_COMMON_GRALLOC := true
BOARD_USES_EXYNOS_GRALLOC_VERSION := 4
BOARD_USES_ALIGN_RESTRICTION := true
BOARD_USES_GRALLOC_ION_SYNC := true

# Graphics
BOARD_USES_EXYNOS_DATASPACE_FEATURE := true

# Storage options
BOARD_USES_BOOT_STORAGE := ufs
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
TARGET_USERIMAGES_USE_F2FS := true
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
BOARD_FLASH_BLOCK_SIZE := 4096

BOARD_KERNEL_CMDLINE := androidboot.selinux=enforce

# WIFI related definition
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/dhd/parameters/firmware_path"
#WIFI_DRIVER_MODULE_PATH     := "/lib/modules/dhd.ko"
WIFI_DRIVER_MODULE_NAME     := "dhd"
WIFI_DRIVER_MODULE_ARG      := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_AP_ARG   := "firmware_path=/system/vendor/etc/wifi/bcmdhd_apsta.bin nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
WIFI_DRIVER_MODULE_P2P_ARG  := "firmware_path=/system/vendor/etc/wifi/bcmdhd_sta.bin_b0 nvram_path=/system/vendor/etc/wifi/nvram_net.txt"
#WIFI_DRIVER_FW_PATH_STA     := "/system/vendor/etc/wifi/bcmdhd_sta.bin"
#WIFI_DRIVER_FW_PATH_P2P     := "/system/vendor/etc/wifi/bcmdhd_sta.bin_b0"
#WIFI_DRIVER_FW_PATH_AP      := "/system/vendor/etc/wifi/bcmdhd_apsta.bin"
#WIFI_DRIVER_FW_PATH_MFG     := "/system/vendor/etc/wifi/bcmdhd_mfg.bin_b0"

# We do not want to overwrite WLAN configurations in universalxxxx/BoardConfig.mk file.

# Choose the vendor of WLAN for wlan_mfg and wifi.c
# 1. Broadcom
# 2. Atheros
# 3. TI
# 4. Qualcomm
WLAN_VENDOR = 1

# Choose the WLAN chipset
# broadcom: bcm4329, bcm4330, bcm4334, bcm43241, bcm4335, bcm4339, bcm4354
# atheros: ar6003x, ar6004, ar6005, ar603x
WLAN_CHIP := bcm4375

# Choose the type of WLAN chipset
# CoB type: COB, Module type: MODULE
WLAN_CHIP_TYPE := MODULE


########################
# Video Codec
########################
# 0. Default C2
BOARD_USE_DEFAULT_SERVICE := false

# 1. Exynos C2
BOARD_USE_CSC_FILTER := true
BOARD_USE_DEC_SW_CSC := false
BOARD_USE_ENC_SW_CSC := false
BOARD_SUPPORT_MFC_ENC_RGB := true
BOARD_USE_BLOB_ALLOCATOR := true
BOARD_USE_PERFORMANCE := true
BOARD_USE_QUERY_HDR2SDR := false
BOARD_USE_COMPRESSED_COLOR := true

# 2. Exynos OMX
BOARD_USE_DMA_BUF := true
BOARD_USE_NON_CACHED_GRAPHICBUFFER := true
BOARD_USE_GSC_RGB_ENCODER := true
BOARD_USE_CSC_HW := false
BOARD_USE_S3D_SUPPORT := false
BOARD_USE_DEINTERLACING_SUPPORT := true
BOARD_USE_HEVCENC_SUPPORT := true
BOARD_USE_HEVC_HWIP := false
BOARD_USE_VP9DEC_SUPPORT := true
BOARD_USE_VP9ENC_SUPPORT := true
BOARD_USE_WFDENC_SUPPORT := true
BOARD_USE_CUSTOM_COMPONENT_SUPPORT := true
BOARD_USE_VIDEO_EXT_FOR_WFD_HDCP := true
BOARD_USE_SINGLE_PLANE_IN_DRM := false
BOARD_USE_WA_ION_BUF_REF := true
BOARD_USE_DIVX_ENABLE := true
########################

#
# AUDIO & VOICE
#
BOARD_USES_GENERIC_AUDIO := true

# Primary AudioHAL ConfigurIation
BOARD_USE_COMMON_AUDIOHAL := true
BOARD_USE_CALLIOPE_AUDIOHAL := false
BOARD_USE_AUDIOHAL := false
BOARD_USE_AUDIOHAL_COMV1 := false
BOARD_USE_AUDIOHAL_COMV2 := true

# RILClient selection configuration 'SIPC' or 'SIT'
BOARD_USE_AUDIOHAL_RILCLIENT := SIT

#  Audio Feature Configuration
BOARD_USE_OFFLOAD_EFFECT := false
BOARD_USE_BTA2DP_OFFLOAD := false

# SoundTriggerHAL Configuration
BOARD_USE_SOUNDTRIGGER_HAL := true
BOARD_USE_SOUNDTRIGGER_HAL_MMAP := true
BOARD_USE_SOUNDTRIGGER_HAL_2_3 := true
BOARD_USE_VTS_FW_LOW_POWER := false

# Audio Logging service
BOARD_USES_AUDIO_LOGGING_SERVICE := true

# CAMERA
BOARD_BACK_CAMERA_ROTATION := 90
BOARD_FRONT_CAMERA_ROTATION := 270
BOARD_SECURE_CAMERA_ROTATION := 0
ifeq ($(TARGET_PRODUCT),full_smdk2100_r)
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5KGM1SP
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K4H5YC
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5K5E9
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
else
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5K2LD
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K3J1
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5KGW2
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_IMX563
BOARD_BACK_2_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
endif

BOARD_CAMERA_USES_DUAL_CAMERA := true
#BOARD_DUAL_CAMERA_REAR_ZOOM_MASTER := CAMERA_ID_BACK_0
#BOARD_DUAL_CAMERA_REAR_ZOOM_SLAVE := CAMERA_ID_BACK_1
#BOARD_DUAL_CAMERA_REAR_PORTRAIT_MASTER := CAMERA_ID_BACK_1
#BOARD_DUAL_CAMERA_REAR_PORTRAIT_SLAVE := CAMERA_ID_BACK_0
#BOARD_DUAL_CAMERA_FRONT_PORTRAIT_MASTER := CAMERA_ID_FRONT_0
#BOARD_DUAL_CAMERA_FRONT_PORTRAIT_SLAVE := CAMERA_ID_FRONT_1

BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_FAKE := true
#BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_ARCSOFT := false

BOARD_CAMERA_USES_3AA_DNG := true
BOARD_CAMERA_USES_SBWC := false
BOARD_CAMERA_USES_SLSI_VENDOR_TAGS := true
BOARD_CAMERA_USES_CLAHE := false
BOARD_CAMERA_USES_BAYER_GDC := false
BOARD_CAMERA_USES_YUVPP := true
BOARD_CAMERA_USES_EXYNOS_GDC := false
BOARD_CAMERA_USES_REMOSAIC_SENSOR := true

#BOARD_CAMERA_USES_LLS_SOLUTION := true
#BOARD_CAMERA_USES_CAMERA_SOLUTION_VDIS := true
BOARD_CAMERA_USES_SLSI_PLUGIN_V1 := true
BOARD_CAMERA_USES_SLSI_PLUGIN_O21 := true
BOARD_CAMERA_USES_EXYNOS_VPL := true
BOARD_CAMERA_USES_EFD := true
#BOARD_CAMERA_USES_EXYNOS_LEC := true
#BOARD_CAMERA_BUILD_SOLUTION_SRC := true
#BOARD_CAMERA_USES_PIPE_HANDLER := true
#BOARD_CAMERA_USES_HIFI_LLS_CAPTURE := true
#BOARD_CAMERA_USES_HIFI_CAPTURE := true
BOARD_CAMERA_USES_HWFC := false
BOARD_CAMERA_TARGET_CLASS := top

# Modules
BOARD_MODULE_USES_GDC := false

# HWComposer
BOARD_HWC_VERSION := libhwc2.1
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := false
#BOARD_HDMI_INCAPABLE := true
TARGET_USES_HWC2 := true
HWC_SKIP_VALIDATE := true
#BOARD_USES_DISPLAYPORT := true
BOARD_USES_EXTERNAL_DISPLAY_POWERMODE := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
BOARD_USES_VSYNC_MODE := true
#BOARD_USES_HDRUI_GLES_CONVERSION := true
#BOARD_USES_DUAL_DISPLAY := true
BOARD_USES_HDR_INTERFACE := true
VSYNC_EVENT_PHASE_OFFSET_NS := 0
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 0
BOARD_USES_HWC_CPU_PERF_MODE := true

SOONG_CONFIG_NAMESPACES += libacryl
SOONG_CONFIG_libacryl += default_scaler
SOONG_CONFIG_libacryl_default_scaler := mscl_votf

BOARD_LIBHDR_PLUGIN := libhdr_plugin_default
BOARD_LIBHDR10P_META_PLUGIN := libhdr10p_meta_plugin_default

# HWCServices
BOARD_USES_HWC_SERVICES := true

# WiFiDisplay
BOARD_USES_VIRTUAL_DISPLAY := true
BOARD_USES_DISABLE_COMPOSITIONTYPE_GLES := true
BOARD_USES_SECURE_ENCODER_ONLY := true

# SCALER
BOARD_USES_DEFAULT_CSC_HW_SCALER := true
BOARD_DEFAULT_CSC_HW_SCALER := 4
BOARD_USES_SCALER_M2M1SHOT := true

# Device Tree
BOARD_USES_DT := true

# PLATFORM LOG
TARGET_USES_LOGD := true

# LIBHWJPEG
TARGET_USES_UNIVERSAL_LIBHWJPEG := true

#FMP
#BOARD_USES_FMP_DM_CRYPT := true
#BOARD_USES_FMP_FSCRYPTO := true
BOARD_USES_METADATA_PARTITION := true

# SKIA
#BOARD_USES_SKIA_MULTITHREADING := true
#BOARD_USES_FIMGAPI_V5X := true

# SELinux Platform Vendor policy for exynos
VENDOR_SEPOLICY := device/samsung/sepolicy/common \
				    device/samsung/sepolicy/exynos2100 \
				    device/samsung/universal2100/sepolicy

ifeq (, $(findstring $(VENDOR_SEPOLICY), $(BOARD_VENDOR_SEPOLICY_DIRS)))
BOARD_VENDOR_SEPOLICY_DIRS += $(VENDOR_SEPOLICY)
endif

# SECCOMP Policy
BOARD_SECCOMP_POLICY = device/samsung/universal2100/seccomp_policy

# SELinux Platform Private policy for exynos
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/samsung/sepolicy/private

# SELinux Platform Public policy for exynos
BOARD_PLAT_PUBLIC_SEPOLICY_DIR := device/samsung/sepolicy/public

#CURL
BOARD_USES_CURL := true

# VISION
# OpenVX-IVA
BOARD_USES_OPENVX := true
# Exynos vision framework (EVF)
#TARGET_USES_EVF := true
# HW acceleration
#TARGET_USES_VPU_KERNEL := true
#TARGET_USES_SCORE_KERNEL := true
#TARGET_USES_CL_KERNEL := false

# exynos RIL
#TARGET_EXYNOS_RIL_SOURCE := true
SUPPORT_NR := true

# SENSOR HUB
BOARD_USES_EXYNOS_SENSORS_DUMMY := true

# GNSS
BOARD_USES_EXYNOS_GNSS_DUMMY := true

TARGET_BOARD_KERNEL_HEADERS := hardware/samsung_slsi/exynos/kernel-5.4-headers/kernel-headers

# SYSTEM SDK
BOARD_SYSTEMSDK_VERSIONS := 30

#VNDK
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION := current

# H/W align restriction of MM IPs
BOARD_EXYNOS_S10B_FORMAT_ALIGN := 64

# Enable AVB2.0
BOARD_AVB_ENABLE := true
BOARD_AVB_ALGORITHM := SHA256_RSA4096
BOARD_AVB_KEY_PATH := device/samsung/universal2100/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_KEY_PATH := device/samsung/universal2100/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 0
BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 0

# Chained partition
BOARD_AVB_BOOT_KEY_PATH := $(BOARD_AVB_KEY_PATH)
BOARD_AVB_BOOT_ALGORITHM := $(BOARD_AVB_ALGORITHM)
BOARD_AVB_BOOT_ROLLBACK_INDEX := 0
BOARD_AVB_BOOT_ROLLBACK_INDEX_LOCATION := 1

BOARD_AVB_VENDOR_BOOT_KEY_PATH := $(BOARD_AVB_KEY_PATH)
BOARD_AVB_VENDOR_BOOT_ALGORITHM := $(BOARD_AVB_ALGORITHM)
BOARD_AVB_VENDOR_BOOT_ROLLBACK_INDEX := 0
BOARD_AVB_VENDOR_BOOT_ROLLBACK_INDEX_LOCATION := 2

BOARD_AVB_DTBO_KEY_PATH := $(BOARD_AVB_KEY_PATH)
BOARD_AVB_DTBO_ALGORITHM := $(BOARD_AVB_ALGORITHM)
BOARD_AVB_DTBO_ROLLBACK_INDEX := 0
BOARD_AVB_DTBO_ROLLBACK_INDEX_LOCATION := 3

BOARD_AVB_VBMETA_SYSTEM := system vendor
BOARD_AVB_VBMETA_SYSTEM_KEY_PATH := $(BOARD_AVB_KEY_PATH)
BOARD_AVB_VBMETA_SYSTEM_ALGORITHM := $(BOARD_AVB_ALGORITHM)
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX := 0
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX_LOCATION := 10

# Extra mount point
BOARD_ROOT_EXTRA_SYMLINKS += /mnt/vendor/efs:/efs
BOARD_ROOT_EXTRA_SYMLINKS += /mnt/vendor/persist:/persist

# LIBSBWC-DPU
SOONG_CONFIG_NAMESPACES += sbwcdpu
SOONG_CONFIG_sbwcdpu := enabled
SOONG_CONFIG_sbwcdpu_enabled := true

# LIBSBWCWRAPPER
SOONG_CONFIG_NAMESPACES += sbwcwrapper
SOONG_CONFIG_sbwcwrapper += sbwcwrapper_priority
SOONG_CONFIG_sbwcwrapper_sbwcwrapper_priority := dpuMscl

# CBD (CP booting deamon)
SOONG_CONFIG_NAMESPACES += cbd
SOONG_CONFIG_cbd := version protocol
SOONG_CONFIG_cbd_version := v3
SOONG_CONFIG_cbd_protocol := sit

#Keymaster
BOARD_USES_KEYMASTER_VER4 := true

# DSP
SOONG_CONFIG_NAMESPACES += dsp 
SOONG_CONFIG_dsp += \
	nnapi

SOONG_CONFIG_dsp_nnapi := false

# Enable Mobicore
BOARD_MOBICORE_ENABLE := true

# RENDERENGINE
# Support SBWC formats 0: DISABLE, 1: ENABLE, 2: ENABLE + SUPPORT LOSSY FORMAT
RENDERENGINE_SUPPORTS_SBWC_FORMAT := 0

# BOOT/VENDOR PATCH LEVEL FOR ROOT_OF_TRUST
BOOT_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)
VENDOR_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)

# To support universal2100 EVT0 only, please use one of below
# ifeq ($(TARGET_PRODUCT),full_universal2100_r_evt0)
# ifeq ($(TARGET_USE_EVT0),true)

ifeq ($(MALI_ON_KEYSTONE),true)
# Allow deprecated BUILD_ module types required to build Mali UMD
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
BUILD_BROKEN_MISSING_REQUIRED_MODULES := true
endif

# Gralloc4
SOONG_CONFIG_NAMESPACES += arm_gralloc
SOONG_CONFIG_arm_gralloc := \
	gralloc_arm_no_external_afbc \
	mali_gpu_support_afbc_basic \
	gralloc_init_afbc \
	gralloc_ion_sync_on_lock \
	dpu_support_1010102_afbc

SOONG_CONFIG_arm_gralloc_gralloc_arm_no_external_afbc := false
SOONG_CONFIG_arm_gralloc_mali_gpu_support_afbc_basic := true
SOONG_CONFIG_arm_gralloc_gralloc_init_afbc := true
SOONG_CONFIG_arm_gralloc_gralloc_ion_sync_on_lock := $(BOARD_USES_GRALLOC_ION_SYNC)
SOONG_CONFIG_arm_gralloc_dpu_support_1010102_afbc := true

# libExynosGraphicbuffer
SOONG_CONFIG_NAMESPACES += exynosgraphicbuffer
SOONG_CONFIG_exynosgraphicbuffer := \
	gralloc_version

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),3)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := three
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := four
endif

# USB (USB gadgethal)
SOONG_CONFIG_NAMESPACES += usbgadgethal
SOONG_CONFIG_usbgadgethal:= exynos_product
SOONG_CONFIG_usbgadgethal_exynos_product := default
