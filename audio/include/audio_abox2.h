/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __EXYNOS_AUDIOPROXY2_ABOX_H__
#define __EXYNOS_AUDIOPROXY2_ABOX_H__

#if defined(__cplusplus)
extern "C" {
#endif

#define ABOX_DEV           "/devices/platform/18c50000.abox"
#define ABOX_SYS_DEV       "/sys/devices/platform/18c50000.abox"
#define ABOX_REGMAP_PATH   "/sys/kernel/debug/regmap/18c50000.abox/"

/* SoC Specific scenario support definition */
#define ABOX_SUPPORT_SPKAMP_PROTECTION          false
#define ABOX_SUPPORT_USB_OFFLOAD                true

// Mixer controls for USB playback loop WDMA configuration
#define MIXER_CTL_ABOX_USB_OUT_LOOP_SAMPLERATE          "ABOX WDMA3 Rate"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_WIDTH               "ABOX WDMA3 Width"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_CHANNEL             "ABOX WDMA3 Channel"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_PERIOD_SZ           "ABOX WDMA3 Period"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_ASRC                "ABOX SPUM ASRC3"

// Mixer control for USB capture loop RDMA configuration
#define MIXER_CTL_ABOX_USB_IN_LOOP_SAMPLERATE           "ABOX RDMA10 Rate"
#define MIXER_CTL_ABOX_USB_IN_LOOP_WIDTH                "ABOX RDMA10 Width"
#define MIXER_CTL_ABOX_USB_IN_LOOP_CHANNEL              "ABOX RDMA10 Channel"
#define MIXER_CTL_ABOX_USB_IN_LOOP_PERIOD_SZ            "ABOX RDMA10 Period"

// BTA2DP offload loopback RDMA path controls
#define MIXER_CTL_ABOX_BTA2DP_RDMA_LOOP_INPUT           "ABOX RDMA4_A"
#define MIXER_SET_ABOX_BTA2DP_COMP_OUTPUT               "MCD_A2DP"

// Mixer controls for USB playback Dualpath loop WDMA configuration
#define MIXER_CTL_ABOX_USB_OUT_DUAL_LOOP_SAMPLERATE     "ABOX WDMA2 Rate"
#define MIXER_CTL_ABOX_USB_OUT_DUAL_LOOP_WIDTH          "ABOX WDMA2 Width"
#define MIXER_CTL_ABOX_USB_OUT_DUAL_LOOP_CHANNEL        "ABOX WDMA2 Channel"
#define MIXER_CTL_ABOX_USB_OUT_DUAL_LOOP_PERIOD_SZ      "ABOX WDMA2 Period"

// Fast stream RDMA Func control for HW fade in/out setting
#define MIXER_CTL_ABOX_FAST_RDMA_FUNC                   "ABOX RDMA6 Func"

/* Audiologging A-box PCM dump point for this project */
#define PCM_DUMP_CNT   14
#define ABOX_NODE_NAME_SZ 5
const char supported_dump[PCM_DUMP_CNT][ABOX_NODE_NAME_SZ] = {"rd0", "rd1", "rd3", "rd4", "rd7", "rdb",
                                                              "wr0", "wr1", "wr2", "wr3", "wr4", "wr6",
                                                              "uou", "uin"};

#if defined(__cplusplus)
}  /* extern "C" */
#endif
#endif  // __EXYNOS_AUDIOPROXY_ABOX_H__
