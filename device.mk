#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_SHIPPING_API_LEVEL := 30

include $(LOCAL_PATH)/BoardConfig.mk

ifneq ($(filter R%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
BOARD_BOOT_HEADER_VERSION := 3
BOARD_GKI_NONAB_COMPAT := true
PRODUCT_USE_DYNAMIC_PARTITIONS := true
#PRODUCT_USE_VIRTUAL_AB := true
BOARD_VENDOR_BOOTIMAGE_PARTITION_SIZE := 0x04000000
PREBUILD_PATH_BASE := vendor/samsung_slsi/exynos2100/prebuilts/$(TARGET_PRODUCT)
CONF_PATH_BASE := device/samsung/universal2100/conf_r
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
AB_OTA_UPDATER := true
AB_OTA_PARTITIONS := \
	system \
	vendor \
	vbmeta
TARGET_NO_RECOVERY := true
BOARD_USES_RECOVERY_AS_BOOT := true
FSTAB_PATH_BASE := device/samsung/universal2100/conf_r/ab
else
FSTAB_PATH_BASE := device/samsung/universal2100/conf_r/non_ab
endif
BOARD_KERNEL_MOUDLES := true
BOARD_USES_SW_GATEKEEPER := false
USE_SWIFTSHADER := false
endif

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts
ifeq ($(wildcard $(BOARD_PREBUILTS)),)
INSTALLED_KERNEL_TARGET := $(PREBUILD_PATH_BASE)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(PREBUILD_PATH_BASE)/dtbo.img
BOARD_PREBUILT_DTB := $(PREBUILD_PATH_BASE)/universal2100.dtb
BOARD_PREBUILT_BOOTLOADER_IMG := $(PREBUILD_PATH_BASE)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
else
PREBUILD_PATH_BASE := $(BOARD_PREBUILTS)
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
endif

ifeq ($(BOARD_KERNEL_MOUDLES),true)
BOARD_PREBUILT_KERNEL_MODULES := $(PREBUILD_PATH_BASE)/modules
BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS := $(PREBUILD_PATH_BASE)/modules_symbols
ifneq ($(wildcard $(PREBUILD_PATH_BASE)/vendor_module_list.cfg),)
PRODUCT_COPY_FILES += $(PREBUILD_PATH_BASE)/vendor_module_list.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/init.insmod.$(TARGET_SOC).cfg
VENDOR_KERNEL_MODULE_MODPROBE_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_module_list.cfg | grep -v '#' | grep 'modprobe|' | sed 's/modprobe|//g')
VENDOR_KERNEL_MODULE_INSMOD_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_module_list.cfg | grep -v '#' | grep 'insmod|' | sed 's/insmod|//g')
VENDOR_KERNEL_MODULE_LIST += $(foreach modules, $(VENDOR_KERNEL_MODULE_MODPROBE_LIST) $(VENDOR_KERNEL_MODULE_INSMOD_LIST),$(notdir $(modules)))
endif
ifneq ($(wildcard $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg),)
PRODUCT_COPY_FILES += $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/init.reorder.$(TARGET_SOC).cfg
VENDOR_REORDER_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg | grep -v "#")
endif
endif

ifeq ($(wildcard $(BOARD_PREBUILTS)),)
PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel \
		device/samsung/universal2100/flashall.sh:$(PRODUCT_OUT)/flashall.sh \
		device/samsung/universal2100/flashall.bat:$(PRODUCT_OUT)/flashall.bat
else
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB) $(BOARD_PREBUILT_KERNEL_MODULES) $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
endif

ifeq ($(BOARD_KERNEL_MOUDLES),true)
# Reordering modules list
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(foreach module,\
	$(VENDOR_REORDER_LIST),\
	$(if $(wildcard $(BOARD_PREBUILT_KERNEL_MODULES)/$(module)),$(BOARD_PREBUILT_KERNEL_MODULES)/$(module),\
	$(warning cannot find $(module), SKIPPING...)))
PRODUCT_COPY_FILES += device/samsung/universal2100/conf/init.insmod.sh:/vendor/bin/init.insmod.sh
PRODUCT_COPY_FILES += $(foreach modules, $(wildcard $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS)/*), $(modules):$(PRODUCT_OUT)/modules_symbols/$(notdir $(modules)))
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(foreach module, $(filter-out $(VENDOR_KERNEL_MODULE_LIST) $(VENDOR_REORDER_LIST), $(notdir $(shell cat $(BOARD_PREBUILT_KERNEL_MODULES)/modules.order))),$(BOARD_PREBUILT_KERNEL_MODULES)/$(module))
BOARD_VENDOR_KERNEL_MODULES += $(foreach module, $(VENDOR_KERNEL_MODULE_MODPROBE_LIST), $(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(module)))
ifneq ($(VENDOR_KERNEL_MODULE_INSMOD_LIST),)
PRODUCT_COPY_FILES += $(foreach modules, $(VENDOR_KERNEL_MODULE_INSMOD_LIST), $(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(modules)):$(modules))
endif
ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_KERNEL_MODULES += $(BOARD_VENDOR_RAMDISK_KERNEL_MODULES)
endif
endif

ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
EXYNOS_BOOT_CONTROL_NAME := android.hardware.boot@1.1-impl-exynos
PRODUCT_PACKAGES += \
	$(EXYNOS_BOOT_CONTROL_HAL) \
	$(EXYNOS_BOOT_CONTROL_HAL).recovery \
	android.hardware.boot@1.1-service \
	update_engine \
	update_verifier
endif

# recovery mode
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_INCLUDE_RECOVERY_DTBO := true
endif

ifneq ($(call math_gt_or_eq,$(BOARD_BOOT_HEADER_VERSION),3),)
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_IMAGE_BASE_ADDRESS := 0x80000000
BOARD_KERNEL_OFFSET := 0x00080000
BOARD_RAMDISK_OFFSET := 0x04000000
BOARD_DTB_OFFSET := 0x0A000000
BOARD_KERNEL_TAGS_OFFSET := 0

ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_MKBOOTIMG_ARGS := \
  --base $(BOARD_IMAGE_BASE_ADDRESS) \
  --kernel_offset $(BOARD_KERNEL_OFFSET) \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version 2 \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset $(BOARD_DTB_OFFSET) \
  --pagesize $(BOARD_FLASH_BLOCK_SIZE) \
  --recovery_dtbo $(BOARD_PREBUILT_DTBOIMAGE)
endif

BOARD_MKBOOTIMG_ARGS := \
  --base $(BOARD_IMAGE_BASE_ADDRESS) \
  --kernel_offset $(BOARD_KERNEL_OFFSET) \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset $(BOARD_DTB_OFFSET) \
  --pagesize $(BOARD_FLASH_BLOCK_SIZE)
else
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION)
endif

PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel

# Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x04000000
else
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x04000000
endif
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000

BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 734003200

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 3145728000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
BOARD_MOUNT_SDCARD_RW := true

ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x08000000
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
endif

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Dynamic Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_SUPER_PARTITION_SIZE := 4194304000
else
BOARD_SUPER_PARTITION_SIZE := 8388608000
endif

# Configuration for dynamic partitions
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 4185915392
ifeq ($(WITH_ESSI),true)
BOARD_GROUP_BASIC_PARTITION_LIST := vendor
else
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif
endif


# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    audio.offload.min.duration.secs=30

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    ro.incremental.enable=module:/vendor/lib/modules/incrementalfs.ko

# Device Manifest, Device Compatibility Matrix for Treble
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
DEVICE_MANIFEST_FILE := \
	device/samsung/universal2100/manifest_abupdate.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/universal2100/manifest_abupdate_legacy_gralloc.xml
endif

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal2100/framework_compatibility_matrix_abupdate.xml
else

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
DEVICE_MANIFEST_FILE := \
	device/samsung/universal2100/manifest.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/universal2100/manifest_legacy_gralloc.xml
endif

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal2100/framework_compatibility_matrix.xml
endif

DEVICE_MATRIX_FILE := \
	device/samsung/universal2100/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_BOOT_STORAGE), ufs)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal2100/overlay-ufsboot
else ifeq ($(BOARD_USES_BOOT_STORAGE), sdboot)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal2100/overlay-sdboot
else ifeq ($(BOARD_USES_BOOT_STORAGE), emmc)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal2100/overlay-emmcboot
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/universal2100/overlay

# Init files
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.exynos2100.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	device/samsung/universal2100/task_profiles.json:$(TARGET_COPY_OUT_VENDOR)/etc/task_profiles.json \
	device/samsung/universal2100/conf/ueventd.exynos2100.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/samsung/universal2100/conf/init.recovery.exynos2100.rc:root/init.recovery.$(TARGET_SOC).rc

# USB init file
ifeq ($(TARGET_PRODUCT),full_smdk2100_r)
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.smdk2100.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc
else
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.exynos2100.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc
endif

# fstab file selection:
# It must be : fstab.{device_name universal2100}.{storage_option ufs}.{if dynamic_partition dp}
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
BOOTUP_STORAGE := $(BOARD_USES_BOOT_STORAGE).dp
else
BOOTUP_STORAGE := $(BOARD_USES_BOOT_STORAGE)
endif

PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC)

#ifeq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/first_stage_ramdisk/fstab.$(TARGET_SOC)
#endif

TARGET_RECOVERY_FSTAB := $(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE)

# Add Recovery boot rc and fstab for GKI recovery booting
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/conf/init.recovery.exynos2100.rc:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/init.recovery.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.exynos2100.usb.rc:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/etc/init/init.$(TARGET_SOC).usb.rc \
	$(FSTAB_PATH_BASE)/fstab.exynos2100.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/etc/recovery.fstab

# Support devtools
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	Development
endif

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
PRODUCT_PACKAGES += \
	tlrpmb

# RPMB test application (only for eng build)
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	bRPMB
endif

# RPMB TA - tlrpmb
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin

# RPMB Unit Test - bRPMB
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_COPY_FILES += \
       vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/bRPMB:$(TARGET_COPY_OUT_VENDOR)/app/bRPMB
endif

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml
endif

# DPU test HAL
#PRODUCT_PACKAGES += dpu_test

# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)

# Memlogger
PRODUCT_PACKAGES += \
	memlogd \
	libmemlogservice \
	MemlogController

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.1\
       android.hardware.fastboot@1.1-impl-mock.exynos2100
endif

# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power-service.exynos

# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl \
	android.hardware.thermal@2.0-service.exynos

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service

# configStore HAL
PRODUCT_PACKAGES += \
    android.hardware.configstore@1.0-service \
    android.hardware.configstore@1.0-impl

#
# Audio HALs
#
PRODUCT_SOONG_NAMESPACES += device/samsung/universal2100/audio
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/modules/libaudio/audiohal_comv2/odm_specific/sitril
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/modules/libaudio/audiohal_comv2/odm_specific/sipcril

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1

# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@6.0-impl \
	android.hardware.audio.effect@6.0-impl \
	android.hardware.soundtrigger@2.3-impl

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default \
	audio.bluetooth.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	device/samsung/universal2100/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# Mixer Path/policy_config/Calliope firware files for AudioHAL
ifeq ($(TARGET_PRODUCT),full_smdk2100_r)
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/audio/config/audio_policy_configuration_smdk2100.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/universal2100/audio/config/mixer_paths_smdk2100.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/sectiongraph_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/sectiongraph_tplg.conf \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/txse1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse1.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/txse2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse2.bin \
	device/samsung/universal2100/firmware/audio/smdk2100_fw/Tfa9874.cnt:$(TARGET_COPY_OUT_VENDOR)/firmware/Tfa9874.cnt
else
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/universal2100/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal2100/firmware/audio/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal2100/firmware/audio/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal2100/firmware/audio/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal2100/firmware/audio/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal2100/firmware/audio/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/universal2100/firmware/audio/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/universal2100/firmware/audio/sectiongraph_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/sectiongraph_tplg.conf \
	device/samsung/universal2100/firmware/audio/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/universal2100/firmware/audio/txse1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse1.bin \
	device/samsung/universal2100/firmware/audio/txse2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse2.bin
endif

# AudioEffectHAL Configuration
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml

# AP SE parameter overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/firmware/audio/APDV_AUDIO_SLSI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/APDV_AUDIO_SLSI.bin \
	device/samsung/universal2100/firmware/audio/APSV_AUDIO_SLSI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/APSV_AUDIO_SLSI.bin \
	device/samsung/universal2100/firmware/audio/AP_AUDIO_SLSI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/AP_AUDIO_SLSI.bin

# VTS firmware overwrite
ifeq ($(BOARD_USE_VTS_FW_LOW_POWER), true)
PRODUCT_COPY_FILES += \
        device/samsung/universal2100/firmware/audio/vts_lp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin
else
PRODUCT_COPY_FILES += \
        device/samsung/universal2100/firmware/audio/vts.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin
endif

#USB Offload support XML
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	device/samsung/universal2100/audio/config/mixer_usb_default.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_default.xml \
	device/samsung/universal2100/audio/config/mixer_usb_white.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_white.xml \
	device/samsung/universal2100/audio/config/mixer_usb_gray.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_gray.xml

#USB Default framework XML (Note: Delete this once above USB-Offload is enabled)
#PRODUCT_COPY_FILES += \
	frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml

# BT A2DP HAL Server & Default Implementations
ifeq ($(BOARD_USE_BTA2DP_OFFLOAD),true)
PRODUCT_PACKAGES += \
        android.hardware.bluetooth.a2dp@1.0-impl-exynos \
        vendor.samsung_slsi.hardware.ExynosA2DPOffload@2.0-impl
else
# PRODUCT_PACKAGES += \
        audio.a2dp.default
endif

PRODUCT_PACKAGES += \
    android.hardware.bluetooth.audio-impl

# Audio Dump Service & Audiologging APK
ifeq ($(BOARD_USES_AUDIO_LOGGING_SERVICE),true)
PRODUCT_PACKAGES += \
    vendor.samsung_slsi.hardware.audio_dump@1.0-service \
    AudioLogging
endif

# AudioEffectHAL library
#ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
#ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
#PRODUCT_PACKAGES += \
	libexynospostprocbundle
#endif
#endif

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# MIDI support native XML
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL), true)
PRODUCT_PACKAGES += \
        sound_trigger.primary.$(TARGET_SOC)
# VoiceTriggerSystem (VTS) HW test application
PRODUCT_PACKAGES_ENG += \
        vtshw-test
endif

# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

# TinyTools for Audio
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    cplay
endif

# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# Gralloc libs
PRODUCT_PACKAGES += libexynosgraphicbuffer_public
ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@4.0-impl \
    android.hardware.graphics.allocator@4.0-service \
    android.hardware.graphics.allocator@4.0-impl
else
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)
endif

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion_exynos \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

# tlwvdrm
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/00060308060501020000000000000000.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tabin

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist

ifeq ($(BOARD_MOBICORE_ENABLE),true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi510
endif
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp

# Camera HAL
 PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service_64 \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
ifeq ($(TARGET_PRODUCT),full_smdk2100_r)
    PRODUCT_COPY_FILES += \
    device/samsung/universal2100/firmware/camera/smdk2100/is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/universal2100/firmware/camera/smdk2100/is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/universal2100/firmware/camera/smdk2100/setfile_gm1sp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gm1sp.bin \
    device/samsung/universal2100/firmware/camera/smdk2100/setfile_4h5yc.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_4h5yc.bin \
    device/samsung/universal2100/firmware/camera/smdk2100/setfile_5e9.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_5e9.bin
else
    PRODUCT_COPY_FILES += \
    device/samsung/universal2100/firmware/camera/is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/universal2100/firmware/camera/is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/universal2100/firmware/camera/setfile_imx563.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_imx563.bin \
    device/samsung/universal2100/firmware/camera/setfile_2ld.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2ld.bin \
    device/samsung/universal2100/firmware/camera/setfile_3j1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3j1.bin \
    device/samsung/universal2100/firmware/camera/setfile_gw2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gw2.bin
endif

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/universal2100/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/universal2100/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/universal2100/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/universal2100/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/universal2100/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
    frameworks/native/data/etc/android.hardware.camera.raw.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.raw.xml \
    frameworks/native/data/etc/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# Epic daemon
PRODUCT_SOONG_NAMESPACES += device/samsung/universal2100/power
PRODUCT_PACKAGES += epic libems_service libexynos_migov libgmc libepic_helper

PRODUCT_COPY_FILES += \
        device/samsung/universal2100/power/epic.json:$(TARGET_COPY_OUT_VENDOR)/etc/epic.json \
        device/samsung/universal2100/power/ems.json:$(TARGET_COPY_OUT_VENDOR)/etc/ems.json \

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
        vendor.samsung_slsi.hardware.epic@1.0-impl \
        vendor.samsung_slsi.hardware.epic@1.0-service

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1

PRODUCT_PROPERTY_OVERRIDES += \
	debug.sf.disable_backpressure=1

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

# set the vsync mode
ifeq ($(BOARD_USES_VSYNC_MODE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        vendor.hwc.exynos.vsync_mode=0
endif

# Set default USB interface
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=mtp,adb

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

# vpl files
PRODUCT_COPY_FILES += \
    device/samsung/universal2100/firmware/camera/libvpl/default_configuration.flm.cfg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.flm.cfg.bin \
    device/samsung/universal2100/firmware/camera/libvpl/OD_V2.1.6_01_26_QVGA.nnc:$(TARGET_COPY_OUT_VENDOR)/firmware/OD_V2.1.6_01_26_QVGA.nnc \
    device/samsung/universal2100/firmware/camera/libvpl/OD_V2.3.7_02_05_VGA.nnc:$(TARGET_COPY_OUT_VENDOR)/firmware/OD_V2.3.7_02_05_VGA.nnc

####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/firmware/mfc_fw_v15.0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# dummy service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/universal2100/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal2100/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
else
# exynos service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/codec2

DEVICE_MANIFEST_FILE += \
	device/samsung/universal2100/manifest_media_c2.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal2100/media_codecs_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_c2.xml \
	device/samsung/universal2100/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.0-service \
    codec2.vendor.base.policy \
    codec2.vendor.ext.policy \
    libExynosC2ComponentStore \
    libExynosC2H264Dec \
    libExynosC2H264Enc \
    libExynosC2HevcDec \
    libExynosC2HevcEnc \
    libExynosC2Mpeg4Dec \
    libExynosC2Mpeg4Enc \
    libExynosC2H263Dec \
    libExynosC2H263Enc \
    libExynosC2Vp8Dec \
    libExynosC2Vp8Enc \
    libExynosC2Vp9Dec \
    libExynosC2Vp9Enc \
    libExynosC2Av1Dec

PRODUCT_PROPERTY_OVERRIDES += \
	debug.stagefright.ccodec_strict_type=true \
	debug.stagefright.ccodec_lax_type=true \
	debug.stagefright.c2-poolmask=917504 \
	debug.stagefright.ccodec_delayed_params=1 \
	ro.vendor.gpu.dataspace=1

ifeq ($(BOARD_USE_COMPRESSED_COLOR), true)
PRODUCT_PROPERTY_OVERRIDES += \
    vendor.debug.c2.sbwc.enable=true
endif
endif

# 2. OpenMAX IL
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_audio.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video.xml \
	device/samsung/universal2100/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/universal2100/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
    device/samsung/universal2100/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

PRODUCT_PACKAGES += \
    libstagefrighthw \
    libExynosOMX_Core \
    libExynosOMX_Resourcemanager \
    libOMX.Exynos.MPEG4.Decoder \
    libOMX.Exynos.AVC.Decoder \
    libOMX.Exynos.WMV.Decoder \
    libOMX.Exynos.VP8.Decoder \
    libOMX.Exynos.HEVC.Decoder \
    libOMX.Exynos.VP9.Decoder \
    libOMX.Exynos.MPEG4.Encoder \
    libOMX.Exynos.AVC.Encoder \
    libOMX.Exynos.VP8.Encoder \
    libOMX.Exynos.HEVC.Encoder \
    libOMX.Exynos.VP9.Encoder \
    libOMX.Exynos.AVC.WFD.Encoder \
    libOMX.Exynos.HEVC.WFD.Encoder
####################################

# 3. FilmGrainNoise
PRODUCT_PACKAGES += \
    libFilmGrainNoise \
    clFGN10BitNV12_32.bin \
    clFGN10BitNV12_64.bin \
    clFGN10BitYV12_32.bin \
    clFGN10BitYV12_64.bin \
    clFGN8BitNV12_32.bin \
    clFGN8BitNV12_64.bin \
    clFGN8BitYV12_32.bin \
    clFGN8BitYV12_64.bin

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/universal2100/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))

PRODUCT_COPY_FILES += device/samsung/universal2100/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# Telephony
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

# EPX firmware overwrite
#PRODUCT_COPY_FILES += \
	device/samsung/universal2100/firmware/epx.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/epx.bin

# Override heap growth limit due to high display density on device
# because this device has xxdpi screen.
# Note that this should be specified before reading XXX-dalvik-heap.mk
PRODUCT_PROPERTY_OVERRIDES +=           \
        dalvik.vm.heapgrowthlimit=256m  \
        dalvik.vm.heapminfree=2m

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

#GPS
# PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.1-impl \
	vendor.samsung.hardware.gnss@1.0-impl \
	vendor.samsung.hardware.gnss@1.0-service

# Exynos OpenVX framework
#PRODUCT_PACKAGES += \
		libexynosvision

#ifeq ($(TARGET_USES_CL_KERNEL),true)
#PRODUCT_PACKAGES += \
       libopenvx-opencl
#endif

#PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.0-service \
	gps.$(TARGET_SOC)

# OFI HAL
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.ofi@2.1-service \
	libofi_dal \
	libofi_kernels_cpu \
	libofi_klm_vendor \
	libofi_klm \
	libofi_plugin \
	libofi_plugin_vendor \
	libofi_rt_framework \
	libofi_rt_framework_user \
	libofi_rt_framework_user_vendor \
	libofi_service_interface \
	libofi_service_interface_vendor \
	libofi_seva \
	libofi_seva_vendor \
	libofib_rt_framework_user \
	libofib_rt_framework \
	libofib_service_interface \
	libinference_engine \
	libinference_engine_vendor \
	libofi_plugin_vendor \
	libofi_plugin \
	libgaussian3x3_gc \
	libnn_gc \
	libenn_dsp_dal \
	libenn_dsp_fw_graph_parser \
	libenn_dsp_kernels_cpu

PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/hardware/ofi/2.1/default vendor/samsung_slsi/exynos/ofi/2.1
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos/enn_driver

PRODUCT_COPY_FILES += \
	device/samsung/universal2100/firmware/dsp/dsp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp.bin \
	device/samsung/universal2100/firmware/dsp/dsp_iac_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm.bin \
	device/samsung/universal2100/firmware/dsp/dsp_iac_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm.bin \
	device/samsung/universal2100/firmware/dsp/dsp_ivp_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm.bin \
	device/samsung/universal2100/firmware/dsp/dsp_ivp_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm.bin \
	device/samsung/universal2100/firmware/dsp/dsp_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_do.bin \
	device/samsung/universal2100/firmware/dsp/dsp_iac_dm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm_do.bin \
	device/samsung/universal2100/firmware/dsp/dsp_iac_pm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm_do.bin \
	device/samsung/universal2100/firmware/dsp/dsp_ivp_dm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm_do.bin \
	device/samsung/universal2100/firmware/dsp/dsp_ivp_pm_do.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm_do.bin \
	device/samsung/universal2100/firmware/dsp/dsp_master.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_master.bin \
	device/samsung/universal2100/firmware/dsp/dsp_reloc_rules.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_reloc_rules.bin \
	device/samsung/universal2100/firmware/dsp/dsp_gkt.xml:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_gkt.xml \
	device/samsung/universal2100/firmware/dsp/libivp.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libivp.elf \
	device/samsung/universal2100/firmware/dsp/liblog.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/liblog.elf \
	device/samsung/universal2100/firmware/dsp/libnn.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libnn.elf \
	device/samsung/universal2100/firmware/dsp/libgaussian3x3.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libgaussian3x3.elf

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# GateKeeper TA
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/07061000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/07061000000000000000000000000000.tlbin

#CryptoManager
PRODUCT_PACKAGES += \
	tlcmdrv \
	tlcmtest \
	cm_test

# CryptoManager - tlcmdrv
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
# tlcmtest / cm_test
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/04010000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/04010000000000000000000000000000.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/cm_test:$(TARGET_COPY_OUT_VENDOR)/app/cm_test
endif

# CryptoManager - tlcmdrv
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/FFFF0000000000000000000000000014.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/ffff0000000000000000000000000014.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/070C0000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/070c0000000000000000000000000000.tlbin \

# ipsec_tunnels feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.ipsec_tunnels.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.ipsec_tunnels.xml

# Eden
PRODUCT_PACKAGES += \
    android.hardware.neuralnetworks@1.3-service.eden-drv \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-service \
    vendor.samsung_slsi.hardware.eden_runtime@1.0 \
    libeden_rt_stub.edensdk.samsung \
    libeden_nn_on_system \
    public.libraries-edensdk.samsung.txt \
    vendor.samsung_slsi.hardware.enn_aux@1.0-impl \
    vendor.samsung_slsi.hardware.enn_aux@1.0-service\
    vendor.samsung_slsi.hardware.enn_aux@1.0

PRODUCT_PROPERTY_OVERRIDES += \
    log.tag.EDEN=INFO \
    ro.vendor.eden.devices=CPU1_GPU1_NPU3_DSP1 \
    ro.vendor.eden.npu.version.path=/sys/devices/platform/npu_exynos/version \
    ro.vendor.eden.core_mask=112

PRODUCT_COPY_FILES += \
	device/samsung/universal2100/eden/eden_kernel_64.bin:$(TARGET_COPY_OUT_VENDOR)/etc/eden/gpu/eden_kernel_64.bin \
	device/samsung/universal2100/firmware/NPU.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/NPU.bin \
	device/samsung/universal2100/eden/enn_preset.json:$(TARGET_COPY_OUT_VENDOR)/etc/eden/enn_preset.json

# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME)

PRODUCT_PACKAGES += \
    vndservicemanager \
    android.hardware.graphics.composer@2.4-impl \
    android.hardware.graphics.composer@2.4-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

PRODUCT_PACKAGES += \
    vendor.samsung_slsi.hardware.SbwcDecompService@1.0-service

PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13100000.ufs/by-name/frp

# SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
	ro.surface_flinger.vsync_event_phase_offset_ns=0 \
	ro.surface_flinger.vsync_sf_event_phase_offset_ns=0 \
	ro.surface_flinger.max_frame_buffer_acquired_buffers=3 \
	ro.surface_flinger.running_without_sync_framework=false \
	ro.surface_flinger.use_color_management=false\
	debug.sf.latch_unsignaled=1 \
	debug.sf.high_fps_late_app_phase_offset_ns=0 \
	debug.sf.high_fps_late_sf_phase_offset_ns=0

# WFD SINK buffer plane info
# this property should be same value with BOARD_USE_SINGLE_PLANE_IN_DRM
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hw.wfd_use_single_plane_in_drm=0

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4

PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin

# strongbox_keymaster_ta
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/00000000000000000000534258505859.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00000000000000000000534258505859.tabin

PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0_strongbox-service

# include strongbox function VTS in case of ENG build
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	strongbox_function_vts
endif

# include strongbox attestation in case of ENG build
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	strongbox_attestation
endif

PRODUCT_PACKAGES += \
	wait_for_dual_keymaster

# include permission to notify that Strongbox is available
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.strongbox_keystore.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.strongbox_keystore.xml

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/ssp/strongbox_keymaster
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/ssp/wait_for_dual_keymaster

# MALI or SWIFTSHADER
ifeq ($(USE_SWIFTSHADER),true)
PRODUCT_PACKAGES += \
	libEGL_swiftshader \
	libGLESv1_CM_swiftshader \
	libGLESv2_swiftshader

PRODUCT_PROPERTY_OVERRIDES += \
       ro.hardware.egl = swiftshader
else

ifneq ($(MALI_ON_KEYSTONE),true)

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos2100/libs
else
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos2100/libs/legacy_gralloc
endif

PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL \
	vulkan.mali \
	vulkan.universal2100_r \
	libgpudataproducer
endif

PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.egl = mali

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2020-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml

endif

# tetheroffload HAL
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.tetheroffload@1.0-service

#vendor directory packages
PRODUCT_PACKAGES += \
	whitelist \
	libstagefright_hdcp \
	libskia_opt

PRODUCT_PACKAGES += \
	mfc_fw.bin \
	dsm.bin \
	NPU.bin

$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)
$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif
# telephony.mk, relevant packages and properties will be defined in
# device-vendor.mk
ifneq ($(TARGET_PRODUCT),full_smdk2100_r)
$(call inherit-product-if-exists, vendor/samsung_slsi/telephony/common/device-vendor.mk)
endif
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos2100/exynos2100.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/O21/hal3/src/camera.mk)
# $(call inherit-product, device/samsung/universal2100/gnss_binaries/gnss_binaries.mk)

## IMSService ##
$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor.mk)

## RCSService ##
# This will be called only if RCSService is building with source code for dev branches.
$(call inherit-product-if-exists, packages/apps/ShannonRcs/device-vendor.mk)

## RCSClient ##
# This will be called only if RcsClient is building with source code for dev branches.
$(call inherit-product-if-exists, packages/apps/RcsClient/device-vendor.mk)

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)

# To support universal2100 EVT0 only, please use one of below
# ifeq ($(TARGET_PRODUCT),full_universal2100_r_evt0)
# ifeq ($(TARGET_USE_EVT0),true)

#Enable protected contents on SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
	ro.surface_flinger.protected_contents=1

PRODUCT_OTA_ENFORCE_VINTF_KERNEL_REQUIREMENTS = false
