adb reboot bootloader

fastboot flash bootloader out/target/product/universal2100/bootloader.img
fastboot flash dtbo out/target/product/universal2100/dtbo.img
fastboot flash boot out/target/product/universal2100/boot.img
fastboot flash vendor_boot out/target/product/universal2100/vendor_boot.img
fastboot flash vbmeta out/target/product/universal2100/vbmeta.img
fastboot flash vbmeta_system out/target/product/universal2100/vbmeta_system.img
fastboot flash recovery out/target/product/universal2100/recovery.img

fastboot reboot fastboot

fastboot flash vendor out/target/product/universal2100/vendor.img
fastboot flash system out/target/product/universal2100/system.img
fastboot reboot -w
