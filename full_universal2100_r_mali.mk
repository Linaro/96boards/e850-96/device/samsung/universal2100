# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for universal2100 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and universal2100, hence its name.
#

MALI_ON_KEYSTONE := true

$(call inherit-product, device/samsung/universal2100/full_universal2100_r.mk)

ifeq ($(MALI_PREBUILT),false)
PRODUCT_PACKAGES += \
	libmali \
	libOpenCL \
	libOpenCL_symlink \
	vulkan.mali \
	libgpudataproducer
else
PRODUCT_SOONG_NAMESPACES += vendor/arm/mali/exynos2100
PRODUCT_PACKAGES += \
	libmali_prebuilt \
	libOpenCL_prebuilt \
	libOpenCL_symlink \
	vulkan.mali_prebuilt \
	libgpudataproducer_prebuilt
endif

#Enable supporting AGI(Android GPU Inspector)
PRODUCT_PROPERTY_OVERRIDES += \
	graphics.gpu.profiler.support=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.vulkan=mali

#Enable protected contents on SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
	ro.surface_flinger.protected_contents=1

PRODUCT_NAME := full_universal2100_r_mali
PRODUCT_DEVICE := universal2100_r_mali
